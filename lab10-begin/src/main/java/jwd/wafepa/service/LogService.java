package jwd.wafepa.service;

import java.util.List;

import jwd.wafepa.model.Log;

public interface LogService {

	Log save(Log log);
	List<Log> findAll();
}
