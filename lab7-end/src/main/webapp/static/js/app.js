wafepaApp = angular.module('wafepaApp', ['ngRoute'])

wafepaApp.controller('ActivityController', function($scope, $http, $location, $routeParams) {
	
	$scope.getActivities = function() {
		$http.get('api/activities')
				.success(function(data) {
					$scope.activities = data;
					$scope.successMessage = 'Everything is ok.';
				})
				.error(function() {
					$scope.errorMessage = 'Oops, something went wrong!';
				});
	};
	
	$scope.deleteActivity = function(id, index) {
		$http.delete('api/activities/' + id)
				.success(function() {
					$scope.activities.splice(index, 1);
				})
				.error(function() {
					
				});
	};
	
	$scope.initActivity = function() {
		$scope.activity = {};
		
		if ($routeParams && $routeParams.id) {
			$http.get('api/activities/' + $routeParams.id)
					.success(function(data) {
						$scope.activity = data;
					});
		}
	};
	
	$scope.saveActivity = function() {
		if ($scope.activity.id) {
			$http.put('api/activities/'+ $scope.activity.id, $scope.activity)
					.success(function() {
						$location.path('/activities');
					});
		}
		else {
			$http.post('api/activities', $scope.activity)
					.success(function() {
						$location.path('/activities');
			});
		}
	};
});

wafepaApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'static/html/home.html'
        })
        .when('/activities', {
            templateUrl : 'static/html/activities.html',
            controller: 'ActivityController'
        })
        .when('/activities/add', {
            templateUrl : 'static/html/addEditActivity.html',
            controller: 'ActivityController'
        })
        .when('/activities/edit/:id', {
            templateUrl : 'static/html/addEditActivity.html',
            controller: 'ActivityController'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);