package jwd.wafepa.service;

import jwd.wafepa.model.Log;

public interface LogService {

	Log save(Log log);
}
