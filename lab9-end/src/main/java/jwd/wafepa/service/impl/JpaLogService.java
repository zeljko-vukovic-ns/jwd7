package jwd.wafepa.service.impl;

import jwd.wafepa.model.Log;
import jwd.wafepa.repository.LogRepository;
import jwd.wafepa.service.LogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class JpaLogService implements LogService {

	@Autowired
	private LogRepository logRepository;
	
	@Override
	public Log save(Log log) {
		return logRepository.save(log);
	}

}
