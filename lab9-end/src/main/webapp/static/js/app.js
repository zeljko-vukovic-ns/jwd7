wafepaApp = angular.module('wafepaApp', ['ngRoute'])

wafepaApp.controller('ActivityController', function($scope, $http, $location, $routeParams) {
	
	$scope.page = 0;
	$scope.pageSize = 5;
	
	$scope.getActivities = function() {
		var request_params = {};
		if ($scope.search) {
			request_params['name'] = $scope.search;
		}
		request_params['page'] = $scope.page;
		request_params['pageSize'] = $scope.pageSize;
		
		$http.get('api/activities', { params : request_params } )
				.success(function(data, status, headers) {
					$scope.activities = data;
					$scope.successMessage = 'Everything is ok.';
					
					$scope.totalPages = headers()['total-pages']
					$scope.totalElements = headers()['total-elements']
				})
				.error(function() {
					$scope.errorMessage = 'Oops, something went wrong!';
				});
	};
	
	$scope.deleteActivity = function(id, index) {
		$http.delete('api/activities/' + id)
				.success(function() {
					$scope.activities.splice(index, 1);
				})
				.error(function() {
					
				});
	};
	
	$scope.initActivity = function() {
		$scope.activity = {};
		
		if ($routeParams && $routeParams.id) {
			$http.get('api/activities/' + $routeParams.id)
					.success(function(data) {
						$scope.activity = data;
					});
		}
	};
	
	$scope.saveActivity = function() {
		if ($scope.activity.id) {
			$http.put('api/activities/'+ $scope.activity.id, $scope.activity)
					.success(function() {
						$location.path('/activities');
					});
		}
		else {
			$http.post('api/activities', $scope.activity)
					.success(function() {
						$location.path('/activities');
			});
		}
	};
	
	$scope.changePage = function(page) {
		$scope.page = page;
		$scope.getActivities();
	}
});

wafepaApp.controller('LogController', function($scope, $http, $location, $routeParams) {
	
	$scope.initLog = function() {
		$scope.log = {};
		
		var request_params = {};
		request_params['page'] = 0;
		request_params['pageSize'] = 5;
		
		$http.get('api/activities', { params : request_params })
				.success(function(data) {
					$scope.activities = data;
				});
	};
	
	$scope.saveLog = function() {
		$scope.log.activity = $scope.activitySelected;
		$http.post('api/logs', $scope.log)
				.success(function() {
					$location.path('/');
				});
	};
	
	$scope.getLogs = function() {
		if ($routeParams && $routeParams.activityId) {
			var request_params = {};
			request_params['activityId'] = $routeParams.activityId;
			
			$http.get('api/logs', { params : request_params})
					.success(function(data) {
						$scope.logs = data;
						$scope.successMessage = 'Everything is ok.';
					});
		}
		
	};
});

wafepaApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'static/html/home.html'
        })
        .when('/activities', {
            templateUrl : 'static/html/activities.html',
            controller: 'ActivityController'
        })
        .when('/activities/add', {
            templateUrl : 'static/html/addEditActivity.html',
            controller: 'ActivityController'
        })
        .when('/activities/edit/:id', {
            templateUrl : 'static/html/addEditActivity.html',
            controller: 'ActivityController'
        })
        .when('/logs/add', {
            templateUrl : 'static/html/addEditLog.html',
            controller: 'LogController'
        })
        .when('/activities/:activityId/logs', {
            templateUrl : 'static/html/logs.html',
            controller: 'LogController'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);